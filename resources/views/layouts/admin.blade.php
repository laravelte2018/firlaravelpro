<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
               <meta name="_token" content="{{ csrf_token() }}">
        <meta name="public_path" content="{{ asset('/') }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Book store Cpanel</title>
        <!-- Bootstrap core CSS-->
        <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Custom fonts for this template-->
        <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <!-- Page level plugin CSS-->
        <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
        @yield('custom_css')
    </head>

    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <a class="navbar-brand" href="{{url('/')}}">Book Store</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Users">
                        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUsers" data-parent="#exampleAccordion">
                            <i class="fa fa-fw fa-wrench"></i>
                            <span class="nav-link-text">Users Management</span>
                        </a>
                        <ul class="sidenav-second-level collapse" id="collapseUsers">
                            <li>
                                <a href="{{route('admin.users.index')}}">View Users</a>
                            </li>
                            <li>
                                <a href="{{route('admin.users.create')}}">Create User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Products">
                        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
                            <i class="fa fa-fw fa-sitemap"></i>
                            <span class="nav-link-text">Products Management</span>
                        </a>
                        <ul class="sidenav-second-level collapse" id="collapseMulti">

                            <li>
                                <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Products</a>
                                <ul class="sidenav-third-level collapse" id="collapseMulti2">
                                    <li>
                                        <a href="{{route('admin.products.index')}}">View Products</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.products.create')}}">Create Product</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Types</a>
                                <ul class="sidenav-third-level collapse" id="collapseMulti2">
                                    <li>
                                        <a href="{{route('admin.types.index')}}">View Types</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.types.create')}}">Create Types</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti3">Languages</a>
                                <ul class="sidenav-third-level collapse" id="collapseMulti3">
                                    <li>
                                        <a href="{{route('admin.languages.index')}}">View Languages</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.languages.create')}}">Create Language</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti4">Authors</a>
                                <ul class="sidenav-third-level collapse" id="collapseMulti4">
                                    <li>
                                        <a href="{{route('admin.authors.index')}}">View Authors</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.authors.create')}}">Create Authors</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti5">Publishers</a>
                                <ul class="sidenav-third-level collapse" id="collapseMulti5">
                                    <li>
                                        <a href="{{route('admin.publishers.index')}}">View Publishers</a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.publishers.create')}}">Create Publisher</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">

                        <a class="nav-link" href="{{url('/ordermanagement')}}">
                            <i class="fa fa-fw fa-link"></i>
                            <span class="nav-link-text">Order Management</span>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav sidenav-toggler">
                    <li class="nav-item">
                        <a class="nav-link text-center" id="sidenavToggler">
                            <i class="fa fa-fw fa-angle-left"></i>
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="content-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fa fa-angle-up"></i>
            </a>
            <!-- Logout Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Bootstrap core JavaScript-->
            <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
            <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
            <!-- Core plugin JavaScript-->
            <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
            <!-- Page level plugin JavaScript-->
            <script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
            <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
            <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
            <!-- Custom scripts for all pages-->
            <script src="{{asset('js/sb-admin.min.js')}}"></script>
            <script src="{{asset('js/js.js')}}"></script>
            <!-- Custom scripts for this page-->
            <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>
                        <script src="{{asset('js/delete_image.js')}}"></script>
            @yield('custom_js')
        </div>
    </body>

</html>

