
@extends('layouts.layout')
@section('content')


<!--************************************
                Header End
*************************************-->
<!--************************************
                Inner Banner Start
*************************************-->
<div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{asset('images/parallax/bgparallax-07.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-innerbannercontent">
                    <h1>All Products</h1>
                    <ol class="tg-breadcrumb">
                        <li><a href="{{asset('home')}}">home</a></li>
                        <li><a href="">Products</a></li>
                        <li class="tg-active">Product Title Here</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>


<!--************************************
                Inner Banner End
*************************************-->
<!--************************************
                Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
    <!--************************************
                    News Grid Start
    *************************************-->
    <div class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 pull-left">
                        <aside id="tg-sidebar" class="tg-sidebar">
                            <div class="tg-widget tg-widgetsearch">
                                <form class="tg-formtheme tg-formsearch">
                                    <div class="form-group">
                                        <button type="submit"><i class="icon-magnifier"></i></button>
                                        <input type="search" name="search" class="form-group" placeholder="Search by title, author, key...">
                                    </div>
                                </form>
                            </div>

                            <div class="tg-widget tg-widgettrending">
                                <div class="tg-widgettitle">
                                    <h3>Recommend Product</h3>
                                </div>
                                 <div class="tg-widgetcontent">
                                    <ul>
                                        @if(count($randomProducts)>0)
                                        @foreach($randomProducts as $prod)

                                        <li>
                                            <article class="tg-post">
                                                <figure><a href="{{asset('product_detail/'.$prod->id)}}"><img id="proByAuthor" src="{{$prod->pictures->last()["url"]?asset($prod->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></a></figure>
                                                <div class="tg-postcontent">
                                                    <div class="tg-posttitle">
                                                        <h3><a href="{{asset('product_detail/'.$prod->id)}}">{{$prod->name}}</a></h3>
                                                    </div>
                                                    <a href="{{asset('author_detail/'.$prod->author->id)}}"><span class="tg-bookwriter">By: {{$prod->author->name}}</span></a>
                                                </div>
                                            </article> 
                                        </li>

                                        @endforeach
                                        @else {{$allproducts}}
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="tg-widget tg-widgetinstagram">
                                <div class="tg-widgettitle">
                                    <h3>Instagram</h3>
                                </div>
                                <div class="tg-widgetcontent">
                                    <ul>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-01.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-02.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-03.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-04.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-05.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-06.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-07.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-08.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-09.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 pull-right">
                        <div class="tg-productgrid">
                            <div class="tg-refinesearch">
                                <span>showing total product</span>
                                <form class="tg-formtheme tg-formsortshoitems">
                                    <fieldset>
                                        <div class="form-group">
                                            <label>sort by:</label>
                                            <span id="filter" class="tg-select" action="{{route('sort_product')}}">
                                                <select>
                                                    <option value="0"></option>
                                                    <option value="1" >decrease</option>
                                                    <option value="2">ascending</option>                                                 
                                                </select>
                                            </span>
                                        </div>

                                    </fieldset>
                                </form>
                            </div>
                            <div class="sort row ">

                            </div>

                            <div class="list-product">

                                @if($allproducts != null)
                                @foreach($allproducts as $product)
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3"  >
                                    <div class="tg-postbook" >

                                        <figure class="tg-featureimg">
                                            <div class="tg-bookimg">
                                                <a  href="{{asset('product_detail/'.$product->id)}}">  
                                                    <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                    <div class="tg-backcover"><img id="imageproduct" src="{{'http://placehold.it/400x400'}}" alt="image description"></div>
                                                </a>      
                                            </div>
                                            <a class="tg-btnaddtowishlist" href="{{asset('product_detail/'.$product->id)}}">
                                                <i class="icon-heart"></i>
                                                <span>View detail</span>
                                            </a>
                                        </figure>
                                        <div class="tg-postbookcontent">
                                            <ul class="tg-bookscategories">
                                                <li><a href="javascript:void(0);">Adventure</a></li>
                                                <li><a href="javascript:void(0);">Fun</a></li>
                                            </ul>
                                            <div class="tg-themetagbox"><span class="tg-themetag">sale</span></div>
                                            <div class="tg-booktitle">
                                                <h3><a href="javascript:void(0);">{{str_limit($product->name,15)}}</a></h3>
                                            </div>
                                            <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author-> name}}</a></span>
                                            <span class="tg-stars"><span></span></span>
                                            <span class="tg-bookprice">
                                                <ins>${{$product->final_price-(($product->promotion_price*$product->final_price)/100  )}}</ins>
                                                <del>${{$product->final_price}}</del>
                                            </span>
                                            <a class="tg-btn tg-btnstyletwo add-to-cart" value="{{$product->id}}" href="javascript:void(0);">
                                                <i class="fa fa-shopping-basket"></i>
                                                <em>Add To Basket</em>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif    

                            </div>
                            <div class="col-lg-6 col-sm-offset-5">
                                {{ $allproducts->render() }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
    </div>

    <script src="{{asset('js/jquery.zoom.js')}}"></script>

    <script type="text/javascript">
$(document).ready(function () {
    $('#ex1').zoom();
    $('#ex2').zoom({on: 'grab'});
    $('#ex3').zoom({on: 'click'});
    $('#ex4').zoom({on: 'toggle'});
});
    </script>
    <!--************************************
                    News Grid End
    *************************************-->






</main>
<!--************************************
                Main End
*************************************-->
<!--************************************
                Footer Start
*************************************-->

@endsection


