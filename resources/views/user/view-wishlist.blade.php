@extends('layouts.user')
@section('content')
@if(isset($wishlists) && $wishlists != null)
<div class="container">
    <div class="row" style="border: 1px solid black">
        <div class="col-md-8" style="border-right: 1px solid black">
            <h4 class="text-center">Products</h4>
        </div>
        <div class="col-md-3" style="border-right: 1px solid black">
            <h4>Price</h4>
        </div>
        <div class="col-md-1">
            <h4>Action</h4>
        </div>
    </div>
    <br>
    @foreach($wishlists as $key => $product)

    <div class="row" style="padding: 10px 10px 10px 10px;">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-4">
                    <img width="150px" class="img-responsive" src="{{asset($product->pictures->first()->url)}}">
                </div>
                <div class="col-md-8">
                    <h5 >{{$product->name}}</h5>
                    <p>Author: {{$product->author->name}}</p>
                    <p>Publisher: {{$product->publisher->name}}</p>
                    <p>Language: {{$product->language->name}}</p>
                    <p>Description: {{$product->description}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <strong><h3>${{$product->final_price-($product->promotion_price * $product->final_price) / 100}}</h3></strong>
        </div>
        <div class="col-md-1">
            <a href="{{url('user/wishlist/delete/').'/'.$product['wish_id']}}"><i class="fa fa-3x fa-remove text-right"></i></a>
        </div>
    </div>
    @endforeach
</div>
@else
<div class="text-center"  >
    <br /><br /><br /><br />
    <h1 ><strong>No product in your wishlist</strong></h1>
</div>
@endif
@stop