@extends('layouts.user')
@section('content')
@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
<h1>My Account</h1>
<div class="row">
    @include('includes.form_error')
</div>

<div class="container">
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th>Order_date</th>
                    <th>Shipper_date</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th></th>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>{{$order->order_date}}</td>
                    <td>{{$order->shipper_date}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->address}}</td>
                    <td>{{$order->phone}}</td>
                    <td style="width: 50px!important;">
                        <button class="btn btn-default btn-success"><a href="{{url('/viewdetails/'.$order->id)}}" style="color: white">Details</a></button>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>    
</div>

@stop