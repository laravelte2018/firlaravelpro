@extends('layouts.user')
@section('content')
@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
<h1>My Account</h1>
<div class="row">
    @include('includes.form_error')
</div>

<div class="container">
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Photo</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>

            </thead>
            <tbody>

                <tr>
                    <td>{{Auth::user()->name}}</td>
                    <td>  <img src="{{Auth::user()->image?asset($users->image): 'http://placehold.it/400x400'}}" width="30" alt="" class="img-responsive img-rounded"></td>
                    <td>{{Auth::user()->address}}</td>
                    <td>{{Auth::user()->email}}</td>
                    <td>{{Auth::user()->phone}}</td>
                    <td style="width: 50px!important;">
                        <button class="btn btn-default btn-success"><a style="color: white;" href="{{url('user/edit')."/".Auth::user()->id}}">Edit</a></button>
                    </td>  
                </tr>

            </tbody>
        </table>
    </div>
  @if (session('response'))
    <div class="alert {{session('response')["status"] == 'success' ?  'alert-success' : 'alert-danger'}}">
        <strong>{{session('response')["message"]}}</strong> 
    </div>
    @endif    
</div>

@stop