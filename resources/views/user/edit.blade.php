@extends('layouts.user')
@section('content')
<h1>Edit Account</h1>
  
<div class="container">
    <div class="row">
        <form action="{{url('user/update',$users->id) }}" method="post" enctype='multipart/form-data' >
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="hidden" name="_method" value="PUT">
            <div class=" form-group ">
                <label for="name">UserName*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ $users->name  }}">
                 <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>   
            <div class=" form-group">
                <label for="address">Address*</label>
                <input type="text" id="address" name="address" class="form-control" value="{{ $users->address}}">
<span class="text-danger">{{ $errors->first('address') }}</span>
            </div>   
            <div class=" form-group ">
                <label for="phone">Phone*</label>
                <input type="number" id="phone" name="phone" class="form-control" value="{{ $users->phone}}">
                  <span class="text-danger">{{ $errors->first('phone') }}</span>
            </div>   
            <div class="  form-group ">
                <label for="email">Email*</label>
                <input type="text" id="email" name="email" class="form-control" value="{{$users->email}}">
                  <span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
            <div class="form-group ">
                <label for="password">{{ __('Password') }}*</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group ">
                <label for="password-confirm">{{ __('Confirm Password') }}*</label>
             
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
             
           </div>
            <div class="form-group {{ $errors->has('photo_id') ? 'has-error' : '' }}">
                <label for="image">Thumnail:</label>
                <input type="file" id="image" name="image" class="form-control" value="{{ old('image') }}">
                <span class="text-danger">{{ $errors->first('image') }}</span>
            </div> 
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Update" />
            </div>
        </form>
    </div>    
</div>
@stop