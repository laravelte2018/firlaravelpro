@extends('layouts.user')
@section('content')
@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
<h1>Order Details</h1>
<style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    .rtl table {
        text-align: right;
    }

    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
</style>
<div class="row">
    @include('includes.form_error')
</div>
<div class="container">
    <div class="invoice-box">
        <table>
            <tr class="top">
                <td>
                    <table>
                        <tr>
                      
                            <td class="title">
                                <img src="{{asset('images/flogo.png')}}" style="width:100%; max-width:300px;">
                            </td>
                      
                            <td>
                            </td>
                            <td>
                                Order_Date<br>
                                Shipping_Date<br>
                            </td>
                            <td>
                                @foreach($orders as $order)
                                {{$order->order_date}}<br>
                                {{$order->shipper_date}}
                                @endforeach
                            </td>
                      
            </tr>
        </table>
        </td>
        </tr>
        </table>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            Name<br>
                            Email<br>
                            Address<br> 
                            Phone
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            @foreach($orders as $order)
                            {{$order->name}}<br>
                            {{$order->email}}<br>
                            {{$order->address}}<br>
                            {{$order->phone}}
                            @endforeach
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                Item
            </td>
            <td></td>
            <td>
                Description
            </td>
            <td></td>
            <td>
                Quantity
            </td>
            <td>
            </td>
            <td>
                Price
            </td>
        </tr>
        <?php $subprice = 0?>
        @foreach($orders_details as $orders_detail)
        <tr class="item">
            
            <td>{{$orders_detail->name}}</td>
            <td></td>
            <td>{{$orders_detail->description}}</td>
            <td></td>
            <td>{{$orders_detail->quantity}}</td>
            <td></td>
            <td>{{$orders_detail->price}}</td>
            <?php $subprice += $orders_detail->price?>
        </tr>
        @endforeach 
        <tr>
            <td colspan="5"></td>
            <td>Total</td>
            <td>{{number_format($subprice,2)}}</td>
        </tr>
    </table>
    </div>  
</div>

@stop