@extends('layouts.layout')
@section('content')
<script type="text/javascript" src="{{asset('/js/jssor.slider.min.js')}}"></script>
<div id="tg-homeslider" class="tg-homeslider tg-haslayout owl-carousel">
    <div class="item" data-vide-options="position: 0% 50%">
        <div class="container">
            <style>
                /* jssor slider loading skin spin css */
                .jssorl-009-spin img {
                    animation-name: jssorl-009-spin;
                    animation-duration: 1.6s;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;
                }

                @keyframes jssorl-009-spin {
                    from {
                        transform: rotate(0deg);
                    }

                    to {
                        transform: rotate(360deg);
                    }
                }

            </style>
            <div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto; width: 1140px; height: 442px; overflow: hidden;">

                <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
                </div>

                <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1140px; height: 442px;
                     overflow: hidden;">
                    <div>
                        <img src="images/author/gf0.jpg" alt="image description">
                    </div>
                    <div>
                        <img src="images/author/banner-4.jpg" alt="image description">
                    </div>
                    <div>
                        <img src="images/author/bia2.jpg" alt="image description">
                    </div>
                    <div>
                        <img src="images/author/banner-1.jpg" alt="image description">
                    </div>
                    <div>
                        <img src="images/author/banner-2.jpg" alt="image description">
                    </div>
                </div>
                <style>
                    .jssorb031 {position:absolute;}
                    .jssorb031 .i {position:absolute;cursor:pointer;}
                    .jssorb031 .i .b {fill:#000;fill-opacity:0.5;stroke:#fff;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.3;}
                    .jssorb031 .i:hover .b {fill:#fff;fill-opacity:.7;stroke:#000;stroke-opacity:.5;}
                    .jssorb031 .iav .b {fill:#fff;stroke:#000;fill-opacity:1;}
                    .jssorb031 .i.idn {opacity:.3;}
                </style>
                <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                    <div data-u="prototype" class="i" style="width:16px;height:16px;">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                        </svg>
                    </div>
                </div>
                <style>
                    .jssora051 {display:block;position:absolute;cursor:pointer;}
                    .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
                    .jssora051:hover {opacity:.8;}
                    .jssora051.jssora051dn {opacity:.5;}
                    .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
                </style>
                <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                    </svg>
                </div>
                <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                    </svg>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->


<!--************************************
                Home Slider End
*************************************-->
<!--************************************
                Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
    <!--************************************
                    All Status Start
    *************************************-->
    <section class="tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-allstatus">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="tg-parallax tg-bgbookwehave" data-z-index="2" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-01.jpg">
                            <div class="tg-status">
                                <div class="tg-statuscontent">
                                    <span class="tg-statusicon"><i class="icon-book"></i></span>
                                    <h2>Books we have

                                        <span> {{$numberProduct}}</span>

                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="tg-parallax tg-bgtotalmembers" data-z-index="2" data-appear-bottom-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-02.jpg">
                            <div class="tg-status">
                                <div class="tg-statuscontent">
                                    <span class="tg-statusicon"><i class="icon-user"></i></span>
                                    <h2>Total members
                                        @if(count($numberUser))
                                        <span> {{count($numberUser)}}</span>
                                        @endif</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="tg-parallax tg-bghappyusers" data-z-index="2" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-03.jpg">
                            <div class="tg-status">
                                <div class="tg-statuscontent">
                                    <span class="tg-statusicon"><i class="icon-heart"></i></span>
                                    <h2>Total product have sale<span>{{$numberproductsale}}</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    All Status End
    *************************************-->
    <!--************************************
                    Best Selling Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>People’s Choice</span>Bestselling Books</h2>
                        <a class="tg-btn" href="{{asset('all_product')}}">View All</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="tg-bestsellingbooksslider" class="tg-bestsellingbooksslider tg-bestsellingbooks owl-carousel">
                        @if($bestSellProducts != null)
                        @foreach($bestSellProducts as $product)	

                        <div class="item">
                            <div class="tg-postbook">
                                <figure class="tg-featureimg">
                                    <div class="tg-bookimg">
                                        <a  href="{{asset('product_detail/'.$product->first()["id"])}}">  
                                            <div class="sale">{{$product->first()["promotion_price"]}}%</div> 
                                            <div class="tg-frontcover"><img id="imageproduct" src="{{$product->first()->pictures->first()->url?asset($product->first()->pictures->first()->url): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                            <div class="tg-backcover"><img id="imageproduct" src="{{$product->first()->pictures->last()->url?asset($product->first()->pictures->last()->url): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                        </a>      
                                    </div>
                                    <a class="tg-btnaddtowishlist" href="{{asset('product_detail/'.$product->first()["id"])}}">
                                        <i class="fa fa-eye"></i>
                                        <span>View detail</span>
                                    </a>
                                </figure>
                                <div class="tg-postbookcontent">
                                    <ul class="tg-bookscategories">
                                        <li><a href="javascript:void(0);">Adventure</a></li>
                                        <li><a href="javascript:void(0);">Fun</a></li>
                                    </ul>
                                    <div class="tg-booktitle">
                                        <h3><a href="javascript:void(0);">{{str_limit($product->first()["name"],15)}}</a></h3>
                                    </div>
                                    <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->first()["author"]-> name}}</a></span>
                                    <span class="tg-stars"><span></span></span>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <span class="tg-bookprice">
                                        <ins>${{$product->first()["final_price"]-(($product->first()["promotion_price"]*$product->first()["final_price"])/100  )}}</ins>
                                        <del>${{$product->first()["final_price"]}}</del>
                                    </span>
                                    <a class="tg-btn tg-btnstyletwo add-to-cart" value="{{$product->first()["id"]}}" href="javascript:void(0);">
                                        <i class="fa fa-shopping-basket"></i>
                                        <em>Add To Basket</em>
                                    </a>

                                </div>
                            </div>
                        </div>


                        @endforeach
                        @endif            


                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--************************************
                    Best Selling End
    *************************************-->
    <!--************************************
                    Featured Item Start
    *************************************-->

    <!--************************************
                    Featured Item End
    *************************************-->
    <!--************************************
                    New Release Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-newrelease">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="tg-sectionhead">
                            <h2><span>Taste The New Spice</span>New Release Books</h2>
                        </div>
                        <div class="tg-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoiars nisiuip commodo consequat aute irure dolor in aprehenderit aveli esseati cillum dolor fugiat nulla pariatur cepteur sint occaecat cupidatat.</p>
                        </div>
                        <div class="tg-btns">
                            <a class="tg-btn tg-active" href="javascript:void(0);">View All</a>
                            <a class="tg-btn" href="javascript:void(0);">Read More</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="tg-newreleasebooks">
                                @foreach($newProducts as $product)
                                <div class="col-xs-4 col-sm-4 col-md-6 col-lg-4">
                                    <div class="tg-postbook">
                                        <figure class="tg-featureimg">
                                            <div class="tg-bookimg">
                                                <a  href="{{asset('product_detail/'.$product->id)}}">  
                                                    <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                    <div class="tg-backcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                </a>
                                            </div>
                                            <a class="tg-btnaddtowishlist" href="{{asset('product_detail/'.$product->id)}}" value="{{$product->id}}">
                                                <i class="icon-eye"></i>
                                                <span>View detail</span>
                                            </a>
                                        </figure>
                                        <div class="tg-postbookcontent">
                                            <ul class="tg-bookscategories">
                                                <li><a href="javascript:void(0);">{{$product->type->name}}</a></li>

                                            </ul>
                                            <div class="tg-booktitle">
                                                <h3><a href="{{asset('product_detail/'.$product->id)}}">{{$product->name}}</a></h3>
                                            </div>
                                            <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author->name}}</a></span>
                                            <span class="tg-stars"><span></span></span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    New Release End
    *************************************-->
    <!--************************************
                    Collection Count Start
    *************************************-->
    <section class="tg-parallax tg-bgcollectioncount tg-haslayout" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-04.jpg">
        <div class="tg-sectionspace tg-collectioncount tg-haslayout">
            <div class="container">
                <div class="row">
                    <div id="tg-collectioncounters" class="tg-collectioncounters">
                        <div class="tg-collectioncounter tg-drama">
                            <div class="tg-collectioncountericon">
                                <i class="icon-bubble"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Drama</h2>
                                <h3 data-from="0" data-to="6179213" data-speed="8000" data-refresh-interval="50">6,179,213</h3>
                            </div>
                        </div>
                        <div class="tg-collectioncounter tg-horror">
                            <div class="tg-collectioncountericon">
                                <i class="icon-heart-pulse"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Horror</h2>
                                <h3 data-from="0" data-to="3121242" data-speed="8000" data-refresh-interval="50">3,121,242</h3>
                            </div>
                        </div>
                        <div class="tg-collectioncounter tg-romance">
                            <div class="tg-collectioncountericon">
                                <i class="icon-heart"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Romance</h2>
                                <h3 data-from="0" data-to="2101012" data-speed="8000" data-refresh-interval="50">2,101,012</h3>
                            </div>
                        </div>
                        <div class="tg-collectioncounter tg-fashion">
                            <div class="tg-collectioncountericon">
                                <i class="icon-star"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Fashion</h2>
                                <h3 data-from="0" data-to="1158245" data-speed="8000" data-refresh-interval="50">1,158,245</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Collection Count End
    *************************************-->
    <!--************************************
                    Picked By Author Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>Some Great Books</span>Picked By Authors</h2>
                        <a class="tg-btn" href="javascript:void(0);">View All</a>
                    </div>
                </div>
                <div id="tg-pickedbyauthorslider" class="tg-pickedbyauthor tg-pickedbyauthorslider owl-carousel">

                    @if($randomProducts != null)
                    @foreach($randomProducts as $product)	


                    <div class="item">
                        <div class="tg-postbook">
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <a  href="{{asset('product_detail/'.$product->id)}}">  
                                        <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                        <div class="tg-backcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                    </a>
                                </div>
                                <div class="tg-hovercontent">
                                    <div class="tg-description">
                                        <p>{{$product->description}}</p>
                                    </div>
                                    <strong class="tg-bookpage">Book Pages:{{$product->page}}</strong>
                                    <strong class="tg-bookcategory">Category:{{$product->type->name}}</strong>
                                    <strong class="tg-bookprice">Price: {{$product->final_price}}</strong>
                                    <div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
                                </div>
                            </figure>
                            <div class="tg-postbookcontent">
                                <div class="tg-booktitle">
                                    <h3><a href="{{asset('product_detail/'.$product->id)}}">{{$product->name}}</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author->name}}</a></span>
                                <a class="tg-btn tg-btnstyletwo add-to-cart" value="{{$product->id}}" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif    
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Picked By Author End
    *************************************-->
    <!--************************************
                    Testimonials Start
    *************************************-->
    <section class="tg-parallax tg-bgtestimonials tg-haslayout" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-05.jpg">
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2">
                        <div id="tg-testimonialsslider" class="tg-testimonialsslider tg-testimonials owl-carousel">
                            @foreach($authors as $author)
                            <div class="item tg-testimonial">
                                <figure><img style="
                                             width: 135px;
                                             height: 135px;
                                             "src="{{$author->image?asset($author->image): 'http://placehold.it/400x400'}}" alt="image description"></figure>
                                <blockquote><q>{{$author->introduce}}</q></blockquote>
                                <div class="tg-testimonialauthor">
                                    <h3>{{$author->name}}</h3>
                                    <span>21,658 Published Books</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Testimonials End
    *************************************-->
    <!--************************************
                    Authors Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>Something New&Hot</span>Sale Products</h2>
                        <a class="tg-btn" href="javascript:void(0);">View All</a>
                    </div>
                </div>
                <div id="tg-authorsslider" class="tg-authors tg-authorsslider owl-carousel" >
                    @foreach($saleProducts as $product)
                    <div class="item" style="width: 165px; margin-right: 30px;">
                        <div class="tg-postbook" >
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <a  href="{{asset('product_detail/'.$product->id)}}">  
                                        <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                        <div class="tg-backcover"><img id="imageproduct" src="{{'http://placehold.it/400x400'}}" alt="image description"></div>
                                    </a>      
                                </div>
                                <a class="tg-btnaddtowishlist" href="{{asset('product_detail/'.$product->id)}}">
                                    <i class="icon-eye"></i>
                                    <span>View detail</span>
                                </a>
                            </figure>
                            <div class="tg-postbookcontent">
                                <ul class="tg-bookscategories">
                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                    <li><a href="javascript:void(0);">Fun</a></li>
                                </ul>
                                <div class="tg-themetagbox"><span class="tg-themetag">sale</span></div>
                                <div class="tg-booktitle">
                                    <h3><a href="javascript:void(0);">{{str_limit($product->name,15)}}</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author-> name}}</a></span>
                                <span class="tg-stars"><span></span></span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="tg-bookprice">
                                    <ins>${{$product->final_price-(($product->promotion_price*$product->final_price)/100  )}}</ins>
                                    <del>${{$product->final_price}}</del>
                                </span>
                                <a class="tg-btn tg-btnstyletwo add-to-cart" value="{{$product->id}}" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Authors End
    *************************************-->
    <!--************************************
                    Call to Action Start
    *************************************-->
    <section class="tg-parallax tg-bgcalltoaction tg-haslayout" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-06.jpg">
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-calltoaction">
                            <ul class="tg-clientservices" style=" margin-left: 129px;">
                                <li class="tg-devlivery">
                                    <span class="tg-clientserviceicon"><i class="icon-rocket"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Fast Delivery</h3>
                                        <p>Shipping Worldwide</p>
                                    </div>
                                </li>
                                <li class="tg-discount">
                                    <span class="tg-clientserviceicon"><i class="icon-tag"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Open Discount</h3>
                                        <p>Offering Open Discount</p>
                                    </div>
                                </li>
                                <li class="tg-quality">
                                    <span class="tg-clientserviceicon"><i class="icon-leaf"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Eyes On Quality</h3>
                                        <p>Publishing Quality Work</p>
                                    </div>
                                </li>
                                <li class="tg-support">
                                    <span class="tg-clientserviceicon"><i class="icon-heart"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>24/7 Support</h3>
                                        <p>Serving Every Moments</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Call to Action End
    *************************************-->

</main>

@endsection

