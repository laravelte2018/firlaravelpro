<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">
        <!-- CSFR token for ajax call -->
        <meta name="_token" content="{{ csrf_token() }}"/>
        <title>Manage Posts</title>
@extends('layouts.admin')
@section('content')
@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
            <div>
                <h1>Manage Products</h1>
                 <button class="btn btn-default btn-success"><a href="{{url('admin/products/create') }}" style="color: white;">Create</a></button><td>
               <br />
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover" id="postTable" style="visibility: hidden;">
                            <thead>
                                <tr>
                                    <th valign="middle">ID</th>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Decription</th>
                                    <th>Publication Date</th>
                                    <th>Pages</th>
                                    <th>Dimensions</th> 
                                    <th>Final_price</th>
                                    <th>Remain</th>  
                                    <th></th>
                                    <th></th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr class="item{{$product->id}}">
                                    <td class="col1">{{$product->id}}</td>
                                    <td><img width="50px" height="50px" class="img-responsive" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt=""></td>
                                    <td>{{$product->name}}</td>
                                    <td>{{str_limit($product->description, 30)}}</td>   
                                    <td>{{$product->publication_date}}</td>
                                    <td>{{$product->page}}</td>
                                    <td>{{$product->dimensions}}</td>
                                    <td>{{$product->final_price}}</td>
                                    <td>{{$product->remain}}</td>          
                                    <td>
                                        <button class="show-modal btn btn-success" data-id="{{$product->id}}" 
                                                data-title="{{$product->name}}" data-type="{{$product->type->name}}"
                                                data-author="{{$product->author->name}}" data-language="{{$product->language->name}}"
                                                data-publisher="{{$product->publisher->name}}" data-date="{{$product->publication_date}}" data-orgin="{{$product->origin_price}}" data-sale="{{$product->promotion_price}}"  data-originquantity="{{$product->origin}}">
                                            <span class="glyphicon glyphicon-eye-open"></span>Details</button>
                                        <button class="btn btn-default btn-success"><a style="color: white;" href="{{   url('admin/products/'.$product->id.'/edit') }}">Edit</a></button><td>
                                        <form action="{{ route('admin.products.destroy',$product->id) }}" method="POST" >
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-danger" value="Delete"/>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.panel-body -->
                    <div class="row">
                        <div style="position: absolute;left: 40%;margin-top: -10px;">
                            {{ $products->render() }}
                        </div>
                    </div>
                </div><!-- /.panel panel-default -->
      

            <!-- Modal form to show a post -->
            <div id="showModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                              <h4 class="modal-title"></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="id">ID:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="id_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="title">Name:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="title_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Type:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="type_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Authors:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="author_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Languages:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="language_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Publisher:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="publisher_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Publication Date:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="date_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Orgin-Price:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="orgin_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Sale(%):</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="sale_show" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="content">Origin Quantity:</label>
                                    <div class="col-sm-10">
                                        <input type="name" class="form-control" id="originquantity_show" disabled>
                                    </div>
                                </div>
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                    <span class='glyphicon glyphicon-remove'></span> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          

            <!-- jQuery -->
            {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> --}}
            <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

            <!-- Bootstrap JavaScript -->
            <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

            <!-- toastr notifications -->
            {{-- <script type="text/javascript" src="{{ asset('toastr/toastr.min.js') }}"></script> --}}
            <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

                    <!-- icheck checkboxes -->
                    <script type="text/javascript" src="{{ asset('icheck/icheck.min.js') }}"></script>

                    <!-- Delay table load until everything else is loaded -->
                   <script>
                        $(window).load(function(){
                            $('#postTable').removeAttr('style');
                        })
                     </script>

    <script>
        $(document).ready(function(){
            $('.published').iCheck({
                checkboxClass: 'icheckbox_square-yellow',
                radioClass: 'iradio_square-yellow',
                increaseArea: '20%'
            });
            $('.published').on('ifClicked', function(event){
                id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::route('changeStatus') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
            $('.published').on('ifToggled', function(event) {
                $(this).closest('tr').toggleClass('warning');
            });
        });
        
    </script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        // add a new post
        $(document).on('click', '.add-modal', function() {
            $('.modal-title').text('Add');
            $('#addModal').modal('show');
        });
        $('.modal-footer').on('click', '.add', function() {
            $.ajax({
                type: 'POST',
                url: 'posts',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'title': $('#title_add').val(),
                    'content': $('#content_add').val()
                },
                success: function(data) {
                    $('.errorTitle').addClass('hidden');
                    $('.errorContent').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#addModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.title) {
                            $('.errorTitle').removeClass('hidden');
                            $('.errorTitle').text(data.errors.title);
                        }
                        if (data.errors.content) {
                            $('.errorContent').removeClass('hidden');
                            $('.errorContent').text(data.errors.content);
                        }
                    } else {
                        toastr.success('Successfully added Post!', 'Success Alert', {timeOut: 5000});
                        $('#postTable').prepend("<tr class='item" + data.id + "'><td class='col1'>" + data.id + "</td><td>" + data.title + "</td><td>" + data.content + "</td><td class='text-center'><input type='checkbox' class='new_published' data-id='" + data.id + " '></td><td>Just now!</td><td><button class='show-modal btn btn-success' data-id='" + data.id + "' data-title='" + data.title + "' data-content='" + data.content + "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-title='" + data.title + "' data-content='" + data.content + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-title='" + data.title + "' data-content='" + data.content + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                        $('.new_published').iCheck({
                            checkboxClass: 'icheckbox_square-yellow',
                            radioClass: 'iradio_square-yellow',
                            increaseArea: '20%'
                        });
                        $('.new_published').on('ifToggled', function(event){
                            $(this).closest('tr').toggleClass('warning');
                        });
                        $('.new_published').on('ifChanged', function(event){
                            id = $(this).data('id');
                            $.ajax({
                                type: 'POST',
                                url: "{{ URL::route('changeStatus') }}",
                                data: {
                                    '_token': $('input[name=_token]').val(),
                                    'id': id
                                },
                                success: function(data) {
                                    // empty
                                },
                            });
                        });
                        $('.col1').each(function (index) {
                            $(this).html(index+1);
                        });
                    }
                },
            });
        });
        //show product
         $(document).on('click', '.show-modal', function () {
                $('.modal-title').text('Show');
                $('#id_show').val($(this).data('id'));
                $('#title_show').val($(this).data('title'));
                $('#type_show').val($(this).data('type'));
                $('#author_show').val($(this).data('author'));
                $('#language_show').val($(this).data('language'));
                $('#publisher_show').val($(this).data('publisher'));
                $('#date_show').val($(this).data('date'));
                $('#orgin_show').val($(this).data('orgin'));
                $('#sale_show').val($(this).data('sale'));
                $('#originquantity_show').val($(this).data('originquantity'));
                $('#showModal').modal('show');});
                
</script>

@stop
                </body>
</html>