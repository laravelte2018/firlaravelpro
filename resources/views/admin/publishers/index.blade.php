@extends('layouts.admin')


@section('content')



<div class="container">
    <h1>Publisher</h1>
    <div class="col-sm-6">
        <form action="{{ route('admin.publishers.create')}}">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Create" />
            </div>
        </form>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>       
                <th>Created at</th>
                <th>Updated date</th>  
                <th>Edit</th>
                <th>Delete</th>
        </thead>
        <tbody>
            @if($publishers)

            @foreach($publishers as $publisher)

            <tr>
                <td>{{$publisher->id}}</td>
                <td>{{$publisher->name}}</a></td>
                <td>{{$publisher->created_at ? $publisher->created_at->diffForHumans() : 'no date'}}</td>
                <td>{{$publisher->updated_at ? $publisher->updated_at->diffForHumans() : 'no date'}}</td>  
                <td>
                    <form action="{{route('admin.publishers.edit',$publisher->id)}}" >
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Edit" />
                        </div>
                    </form>

                </td> 
                <td>  
                    <form action="{{ route('admin.publishers.destroy', $publisher->id) }}" method="POST" >
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <input type="submit" class="btn btn-danger" value="Delete" />

                        </div>
                    </form>
                </td>
            </tr>
            @endforeach

            @endif

        </tbody>
    </table>
    @if (session('response'))
    <div class="alert {{session('response')["status"] == 'success' ?  'alert-success' : 'alert-danger'}}">
    <strong>{{session('response')["message"]}}</strong> 
  </div>
    @endif
    <div class="row">
        <div class="col-lg-6 col-sm-offset-5">
            {{$publishers->render()}}

        </div>
    </div>
</div>

</div>




@stop