
@extends('layouts.admin')
@section('content')
@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
<div class="row">
    <div class="col-sm-9">
        <h1>Order</h1>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Order Id</th>
            <th>Order Date</th>
            <th>Shipper Date</th>
            <th>Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Phone</th>
            <th></th>
    </thead>
    <tbody>
        @foreach($orders as $order)
        <tr>
            <td>{{$order->id}}</td>
            <td>{{$order->order_date}}</td>
            <td>{{$order->shipper_date}}</td>
            <td>{{$order->name}} </a> </td>
            <td>{{$order->address}}</td>
            <td>{{$order->email}}</td>
            <td>{{$order->phone}}</td>
<!--            <td>{{$order->status == 0 ? "Processing" :($order->status == 1 ? "Submited" : ($order->status == 2 ? "Assigned" : "Cancel"))}}</td>-->
            <td>
                <select class="form-control order-status"  order-id="{{$order->id}}">
                    <option value="0" {{$order->status == 0 ? "checked" : ""}}>Processing</option>
                    <option value="1" {{$order->status == 1 ? "checked" : ""}}>Submited</option>
                    <option value="2" {{$order->status == 2 ? "checked" : ""}}>Assigned</option>
                    <option value="3" {{$order->status == 3 ? "checked" : ""}}>Cancel</option>
                </select>
            </td>
            <td style="width: 50px!important;">
                <button class="btn btn-default btn-success"><a style="color: white;" href="{{route('ordermanagement.show',$order->id)}}">Details</a></button>
            </td>  
        </tr>
        @endforeach  

    </tbody>

</table>
<div class="row">
    <div style="position: absolute;left: 50%;">
        {{$orders->render()}}
    </div>

</div>
@stop