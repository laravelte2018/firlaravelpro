
@extends('layouts.layout')
@section('content')
<div class="pg-header">
    <div class="container">
        <br><br>
        <div class="row">
            <div class="col-md-8 col-sm-12 title">
                <h2>Shopping Cart</h2>
                @if(session()->has('cart'))

                <br>

                <a href="{{url('delete-cart')}}" class="remove-cart"><h5 class="text-right btn btn-default btn-warning"><strong>Clear All</strong></h5></a>
                 <a href="{{url('checkout')}}" class="remove-cart"><h5 class="text-right btn btn-default btn-warning"><strong>Order</strong></h5></a>

                <br><br>

                @endif
            </div>
        </div>
    </div>
</div>
<div class="pg-body">
    <div class="container">
        <div class="row">
            @if(session()->has('cart'))
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu" style="border-bottom: 1px solid #d8cfcf">
                            <td class="image"><h4><strong>Image</strong></h4></td>
                            <td class="description"><h4><strong>Book name</strong></h4></td>
                            <td class="price"><h4><strong>Price</strong></h4></td>
                            <td class="quantity"><h4><strong>Quantity</strong></h4></td>
                            <td class="total"><h4><strong>Total</strong></h4></td>
                            <td class="delete"><h4><strong>Delete</strong></h4></td>
                        </tr>
                    </thead>
                    <tbody class="list-cart-product">
                        @foreach(session()->get('cart')->items as $key => $item)
                        <tr>
                            <td class="cart_product">
                                <a href=""><img style="max-width:60%; height: 40%;" width="150" src="{{$item["item"]->pictures->last()["url"]}}" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="{{url('/product_detail').'/'. $item["item"]->id}}">{{$item["item"]->name}}</a></h4>
                                <p>Size: {{$item["item"]->dimensions}}</p>
                            </td>
                            <td class="cart_price">
                                <p>$<strong>{{$item["price"]}}</strong> </p>
                            </td>
                            <td class="cart_quantity">
                                <button class="cart_quantity_button">
                                    <a class="cart_quantity_down cart-btn-minus" href="javascrip:void(0);">-</a>
                                    <input class="cart_quantity_input count" type="text" name="quantity"  pid="{{$item["item"]->id}}" value="{{$item["quantity"]}}" autocomplete="off" size="2">
                                    <a class="cart_quantity_up cart-btn-plus" href="javascrip:void(0);">+</a>
                                </button>
                            </td>
                            <td class="cart_total">
                                <p>$<strong class="subprice-{{$item["item"]->id}}">{{$item["subprice"]}}</strong></p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete remove-cart-product" href="javascript:void(0)" value="{{$item["item"]->id}}"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <h2><strong>No products in your cart</strong></h2>
            <a class="btn btn-default btn-warning" href="{{url('/')}}">Back to Homepage</a>
            <br><br>
            @endif
        </div>
    </div>
</div> <!-- pg-body -->
@endsection

