@extends('layouts.layout')
@section('content')
<div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-07.jpg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-innerbannercontent">
                    <h1>Contact Us</h1>
                    <ol class="tg-breadcrumb">
                        <li><a href="javascript:void(0);">home</a></li>
                        <li class="tg-active">Contact Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--************************************
                Inner Banner End
*************************************-->
<!--************************************
                Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
    <!--************************************
                    Contact Us Start
    *************************************-->
    <div class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-contactus">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-sectionhead">
                            <h2><span>Say Hello!</span>Get In Touch With Us</h2>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.1140565728233!2d108.24235460060713!3d16.059570061295826!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3142177f2ced6d8b%3A0xe282c779264f7088!2zVHLGsOG7nW5nIENhbyDEkOG6s25nIE5naOG7gSDEkMOgIE7hurVuZw!5e0!3m2!1svi!2s!4v1522671558006" width="600" height="750" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        @if(session('message'))
                        <div class="alert alert-success">
                            <strong>Success!</strong> Your feedback has been send successful.
                        </div>
                        @endif
                        <div id="contact-form">
                            <form class="tg-formtheme tg-formcontactus" action="{{url('send-feed-back')}}" method="post">
                                <fieldset>
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group tg-hastextarea">
                                        <input type="text" name="name" class="form-control" placeholder="Full Name (*)">
                                    </div>
                                    <div class="form-group tg-hastextarea">
                                        <input type="email" name="email" class="form-control" placeholder="Email (*)">
                                    </div>
                                    <div class="form-group tg-hastextarea">
                                        <textarea placeholder="Comment (*)" name="content"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="tg-btn tg-active" id="btn-send-feedback">Submit</button>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                       
                        
  
                       
                        <div class="tg-contactdetail">
                            <div class="tg-sectionhead">
                                <h2>Get In Touch With Us</h2>
                            </div>
                            <ul class="tg-contactinfo">
                                <li>
                                    <i class="icon-apartment"></i>
                                    <address>101B Le Huu Trac st, Son Tra district, Da Nang city, VietNam</address>
                                </li>
                                <li>
                                    <i class="icon-phone-handset"></i>
                                    <span>
                                        <em>1900 5656 73</em>
                                    </span>
                                </li>
                                <li>
                                    <i class="icon-clock"></i>
                                    <span>Serving 7 Days A Week From 8am - 5:30pm</span>
                                </li>
                                <li>
                                    <i class="icon-envelope"></i>
                                    <span>
                                        <em><a href="mailto:support@book-store.com">support@bookstore.com</a></em>
                                    </span>
                                </li>
                            </ul>
                            <ul class="tg-socialicons">
                                <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                                <li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                                <li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--************************************
                    Contact Us End
    *************************************-->
</main>
<!--************************************
                Main End
*************************************-->
@stop
