
@extends('layouts.layout')


@section('content')
<div>
    <?php
    echo session('alert');
    ?>
</div>
<div class="row">

    <section id="cart_items">
        <div class="container">
          <center> <h3>Please check and confim your information before click </h3></center>
            @if(!Auth::user())
            <div class="register-req ">
                <p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
            </div><!--/register-req-->
            @endif
            
            <div class="shopper-informations">
                <div class="row">
                    @if(Auth::user())
                  <form action="{{ route('checkout.store') }}" method="post" class="form-horizontal" enctype='multipart/form-data'>
                         <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="col-sm-5" style="background: #f0f0e9">
                                <div class="shopper-info "  >
                            <p>User Information</p>
                            <div class="form-group">
                                <input type="text"  name="" value="{{Auth::user()->name}}" placeholder="User Name" readonly="readonly">	
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="" placeholder="Email*" value="{{Auth::user()->email}}" >
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group">
                                <input type="text" name="" placeholder="Address"value="{{Auth::user()->address}}" >
                                <span class="text-danger">{{ $errors->first('address') }}</span>                            
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone" placeholder="Phone *" value="{{Auth::user()->phone}}" >
                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-7 clearfix">
                            <div class="bill-to">
                                <p>You Can Change Receiver Information </p>
                                <div class="">

                                         <div class="form-group">
                                        <div class="col-md-3">
                                            <label>Email</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" name="email" placeholder="Email*" value="{{Auth::user()->email}}" >
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group"><div class="col-md-3">
                                            <label>Full Name</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" name="name" placeholder="Full Name *" value="{{Auth::user()->name}}" >
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label>Address</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" name="address" placeholder="Address"value="{{Auth::user()->address}}" >
                                            <span class="text-danger">{{ $errors->first('address') }}</span>
                                            </ <div class="form-group">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>Phone</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="phone" placeholder="Phone *" value="{{Auth::user()->phone}}" >
                                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    </div>
                                    
                                          <div class="form-group">
                                        <input type="submit" class="btn btn-success" value="Order " />
                                    </div>
                    </form>
                   
                                    @endif
                                </div>
                            </div>
                            
                             <div class="review-payment">
                        <h2>Review & Payment</h2>
                    </div>

                    <div class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="image">Image</td>
                                    <td class="description">Book name</td>
                                    <td class="price">Price</td>
                                    <td class="quantity">Quantity</td>
                                    <td class="total">Total</td>
                                    <td class="delete"> Delete</td>
                                </tr>
                            </thead>
                            @if(session()->has('cart'))
                            <tbody>

                                @foreach(session()->get('cart')->items as $key => $item)
                                <tr>
                                    <td class="cart_product">
                                        <a href="" ><img class="img-thumbnail" width="150"  src="{{$item["item"]->pictures->last()["url"]}}" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href="{{url('/product_detail').'/'. $item["item"]->id}}">{{$item["item"]->name}}</a></h4>
                                        <input type="hidden" name="book_name" value="{{$item["item"]->id}}" >
                                    </td>
                                    <td class="cart_price">
                                        <p><strong> ${{$item["price"]}}</strong></p>
                                    </td>
                                    <td class="cart_quantity">
                                        <button class="cart_quantity_button">
                                            <a class="cart_quantity_up cart-btn-plus" href="javascrip:void(0);"><i class="text-warning fa fa-plus"></i></a>
                                            <input class="cart_quantity_input count" type="text" name="quantity"  pid="{{$item["item"]->id}}" value="{{$item["quantity"]}}" autocomplete="off" size="2">
                                            <input type="hidden" name="quantity" value="{{$item["quantity"]}}" >
                                            <a class="cart_quantity_down cart-btn-minus" href="javascrip:void(0);"><i class="fa fa-minus"></i></a>
                                        </button>
                                    </td>
                                    <td class="cart_total">
                                        <p class="subprice-{{$item["item"]->id}}">$<strong>{{$item["subprice"]}}</strong></p>
                                         <input type="hidden" name="price" value="{{$item["subprice"]}}" >
                                    </td>
                              
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>



                                @endforeach
                                    <tr>
                                <td colspan="4">&nbsp;</td>
                                <td colspan="2">
                                    <table class="table table-condensed total-result">
                                        <tr>
                                            <td>Cart Sub Total</td>
                                            <td class="cart-total-price">${{session()->has('cart') && session('cart') != null ? session()->get('cart')->totalPrice : 0}}</td>
                                        </tr>
                                        <tr>
                                            <td>Exo Tax</td>
                                            <td>10%</td>
                                        </tr>
                                        <tr class="shipping-cost">
                                            <td>Shipping Cost</td>
                                            <td>Free</td>										
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td class="total-price"><span>${{(0.1   *( session()->has('cart') && session('cart') != null ? session()->get('cart')->totalPrice : 0)+(session()->has('cart') && session('cart') != null ? session()->get('cart')->totalPrice : 0))}}</span></td>
                                        </tr>
                                    </table>
                                </td>
                        <div class="form-group">
                            <div class="col-md-1">
                            </div>


                        </div>
                        </tr>
                            </tbody>


                            @endif  
                        </table>
                    </div>
                      
                    <!--			<div class="payment-options">
                                                            <span>
                                                                    <label><input type="checkbox"> Direct Bank Transfer</label>
                                                            </span>
                                                            <span>
                                                                    <label><input type="checkbox"> Check Payment</label>
                                                            </span>
                                                            <span>
                                                                    <label><input type="checkbox"> Paypal</label>
                                                            </span>
                      
                    </div>-->


                </div>
                </section> <!--/#cart_items-->

            </div>  

        </div>

        @endsection