<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class WishlistsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        if (DB::table('wishlists')->get()->count() == 0) {
            $faker = Faker::create();
            foreach (range(1, 50) as $index) {
                DB::table('wishlists')->insert([
                    'book_id' => $faker->numberBetween(1, 50),
                    'user_id' => $faker->numberBetween(1, 4),
                ]);
            }
        }
    }

}
