<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
  
    {
         if (DB::table('languages')->get()->count() == 0) {
            $languages = [
                [
                    'name' => 'Tiếng anh',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ],[
                    'name' => 'Tiếng việt',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ]
                ];
            DB::table('languages')->insert($languages);
	}
    }
}
