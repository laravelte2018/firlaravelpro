<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class Order_ProductsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (DB::table('order_products')->get()->count() == 0) {

            $faker = Faker::create();
            foreach (range(1, 200) as $index) {
                DB::table('order_products')->insert([
                    'quantity' => $faker->numberBetween(1,20),
                    'price' => $faker->numberBetween(100,1000),
                    'order_id' => $faker->numberBetween(1, 50),
                    'book_id' => $faker->numberBetween(1, 50),
                ]);
            }
        }
    }

}
