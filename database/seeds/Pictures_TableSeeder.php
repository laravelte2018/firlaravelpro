<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class Pictures_TableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (DB::table('pictures')->get()->count() == 0) {
            foreach (range(1, 50) as $index) {
                DB::table('pictures')->insert([
                    'url' => 'images/demo/books/'.$index.'.jpg',
                    'book_id' => $index,
                ]);
            }
            
        }
    }

}
