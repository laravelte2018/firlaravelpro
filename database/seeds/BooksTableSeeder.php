<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BooksTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (DB::table('books')->get()->count() == 0) {
            $faker = Faker::create();
            foreach (range(1, 50) as $index) {
                DB::table('books')->insert([
                    'name' => "Book " . $index,
                    'description' => "none",
                    'language_id' => $faker->numberBetween(1, 2),
                    'author_id' => $faker->numberBetween(1, 7),
                    'publisher_id' => $faker->numberBetween(1, 4),
                    'type_id' => $faker->numberBetween(1, 8),
                    'page' => $faker->numberBetween(200, 1000),
                    'dimensions' => '21x26',
                    'publication_date' => $faker->dateTime,
                    'origin_price' => 100,
                    'promotion_price' => $faker->numberBetween(0, 10),
                    'final_price' => $faker->numberBetween(100, 200),
                    'origin' => 50,
                    'remain' => 50,
                ]);
            }
        }
    }

}
