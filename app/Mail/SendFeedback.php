<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendFeedback extends Mailable {

    use Queueable,
        SerializesModels;

    public $message = "";
    public $mailFrom = "";
    public $mailTo = "";
    public $subject = "";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailTo, $mailFrom, $subject, $message) {
        $this->mailTo = $mailTo;
        $this->mailFrom = $mailFrom;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        //
    }

    public function sendMail() {
        $data = array('name' => "Book Store", "body" => $this->message);
        try {
            Mail::send('email', $data, function ($message) {

                $message->from($this->mailFrom, 'Book Store');
                $message->to($this->mailTo)->subject($this->subject);
            });
        } catch (Exception $exc) {
            echo FALSE;
        }
    }

}
