<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $fillable = [
        'order_date',
        'shipper_date',
        'email',
        'name',
        'address',
        'phone',
        'user_id',
        'status'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function order_products() {
        return $this->belongto('App\Order_Product');
    }
    public function books(){
       return $this->belongsToMany('App\Books','order_products','order_id','book_id');
   }
    
}
