<?php

namespace App;

class Cart {

    public $items;
    public $totalQuantity = 0;
    public $totalPrice = 0;
    public $totalProduct = 0;

    public function __construct($oldCart) {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQuantity = $oldCart->totalQuantity;
            $this->totalPrice = $oldCart->totalPrice;
            $this->totalProduct = $oldCart->totalProduct;
        } else {
            $this->items = null;
        }
    }

    public function add($item, $id) {
        $storedItem = ['quantity' => 0, 'price' => $item->final_price, 'item' => $item];
        $price = number_format($item->final_price-(($item->promotion_price*$item->final_price)/100),2);
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            } else {
                $this->totalProduct++;
            }
        }
        if ($storedItem['quantity'] < 5 && $storedItem['quantity'] <= $item->remain){
             $storedItem['quantity'] ++;
             $this->totalQuantity++;
             $this->totalPrice += $price;
        }
        $storedItem['price'] = $price;
        $storedItem['subprice'] = number_format($storedItem['price'] * $storedItem['quantity'],2);
        $this->items[$id] = $storedItem;
        
        if ($this->totalQuantity == 1)
            $this->totalProduct = $this->totalQuantity;
        
    }

    public function changeProductQuantity($id, $quantity) {
       $oldItem = $this->items[$id];
       $this->items[$id]['quantity'] = $quantity;
       $this->items[$id]['subprice'] = number_format($this->items[$id]['quantity']*$this->items[$id]['price'],2);
       $this->totalQuantity += ($quantity - $oldItem['quantity']); 
       $this->totalPrice += number_format((($this->items[$id]['quantity'] * $this->items[$id]['price']) - ($oldItem['quantity'] * $oldItem['price'])),2);
       
    }

    public function removeItem($id) {
        $this->totalQuantity -= $this->items[$id]['quantity'];
        $this->totalPrice -= number_format($this->items[$id]['subprice'],2);
        $this->totalProduct--;
        unset($this->items[$id]);
    }

}
