<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = [
        'url',
        'book_id'
    ];
    public function book(){

        
        return $this->belongsTo('App\Book');


    }
}
