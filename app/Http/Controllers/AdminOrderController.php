<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Order_product;
use Illuminate\Support\Facades\DB;

class AdminOrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeOrderStatus(Request $request) {
        $order = Order::findOrFail($request->order_id);
        $order->status = $request->status;
        if($order->status == 0 ||$order->status == 1 ||$order->status == 2 ||$order->status == 3 ){
            $order->save();
        }
        $response = array();
        if($order){
            $response[] = true;
        }else{
            $response[] = false;
        }
        return response()->json($response);
    }
    
    public function index() {
        $orders = Order::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.order.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show($id) {
        $orders = Order::where('id', '=', $id)->get();
        $orders_details = DB::table('order_products as or')
                        ->select('orders.order_date', 'orders.shipper_date', 'orders.email', 'orders.phone', 'or.quantity', 'or.price', 'books.name', 'books.description')
                        ->join('orders', 'orders.id', '=', 'or.order_id')
                        ->join('books', 'books.id', '=', 'or.book_id')
                        ->where('order_id', '=', $id)->paginate(4);
        return view('admin.order.show', ['orders_details' => $orders_details, 'orders' => $orders]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
