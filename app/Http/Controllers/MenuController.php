<?php
namespace App\Http\Controllers;

use App\Comment;
use App\Book;
use App\Language;
use App\Publisher;
use App\Author;
use App\Picture;
use App\User;
use App\Type;
use App\Order_product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Mail\SendFeedback;

class MenuController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    public function menuAuthors($id) {
        $types = Type::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $authors = Author::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $languages = Language::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $publishers = Publisher::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $image = Picture::where('book_id', '=', $id)->orderBy('created_at', 'desc')->paginate(4);
        
        $user = Auth::user();
        $comments = Comment::where('book_id', '=', $id)->orderBy('created_at', 'desc')->paginate(6);
        $type = Type::pluck('name', 'id')->all();
        $language = Language::pluck('name', 'id')->all();
        $author = Author::pluck('name', 'id')->all();
        $publisher = Publisher::pluck('name', 'id')->all();
        $product = Book::with('pictures')->findOrFail($id);
        
        
        $getidauthor = $product->author_id;
        $proByAuthor = Book::with('pictures')->orderBy('created_at', 'desc')->where('author_id', '=', $getidauthor)->paginate(4);


        $allAuthors = Book::with('pictures')
                        ->where('author_id', '=', $id)
                        ->groupBy('id')
                        ->orderBy('books.id', 'desc')->paginate(8);

        $allproducts = Book::with('pictures')
                        ->where('is_thumbnail', '=', '0')
                        ->orderBy('books.id', 'desc')->paginate(4);

        return view('author_detail', compact('image', 'product', 'allAuthors','allproducts', 'user','comments','types', 'proByAuthor', 'authors', 'languages', 'publishers', 'type', 'author', 'language', 'publisher'));
    }

    public function menuTypes($id) {
        $types = Type::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $authors = Author::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $languages = Language::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $publishers = Publisher::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $image = Picture::where('book_id', '=', $id)->orderBy('created_at', 'desc')->paginate(4);
        
        $type = Type::pluck('name', 'id')->all();
        $language = Language::pluck('name', 'id')->all();
        $author = Author::pluck('name', 'id')->all();
        $publisher = Publisher::pluck('name', 'id')->all();
        $product = Book::with('pictures')->findOrFail($id);
        
        
        $allTypes = Book::with('pictures')
                        ->where('type_id', '=', $id)
                        ->groupBy('id')
                        ->orderBy('books.id', 'desc')->paginate(8);
        
        
        $allproducts = Book::with('pictures')
                        ->where('is_thumbnail', '=', '0')
                        ->orderBy('books.id', 'desc')->paginate(4);


        return view('type_detail', compact('image', 'product', 'allTypes', 'types', 'allproducts', 'proByAuthor', 'authors', 'languages', 'publishers', 'type', 'author', 'language', 'publisher'));
    }

    public function menuLanguages($id) {
        $types = Type::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $authors = Author::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $languages = Language::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $publishers = Publisher::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $image = Picture::where('book_id', '=', $id)->orderBy('created_at', 'desc')->paginate(4);

        $type = Type::pluck('name', 'id')->all();
        $language = Language::pluck('name', 'id')->all();
        $author = Author::pluck('name', 'id')->all();
        $publisher = Publisher::pluck('name', 'id')->all();
        $product = Book::with('pictures')->findOrFail($id);
       
        
        
        $allLanguages = Book::with('pictures')
                        ->where('language_id', '=', $id)
                        ->groupBy('id')
                        ->orderBy('books.id', 'desc')->paginate(8);
        
       $allproducts = Book::with('pictures')
                        ->where('is_thumbnail', '=', '0')
                        ->orderBy('books.id', 'desc')->paginate(4);

        return view('language_detail', compact('image', 'product','allproducts', 'allLanguages', 'types', 'proByAuthor', 'authors', 'languages', 'publishers', 'type', 'author', 'language', 'publisher'));
    }

    public function menuPublishers($id) {
        $types = Type::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $authors = Author::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $languages = Language::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $publishers = Publisher::where('is_thumbnail', '=', '0')
                ->paginate(5);
        $image = Picture::where('book_id', '=', $id)->orderBy('created_at', 'desc')->paginate(4);

        $type = Type::pluck('name', 'id')->all();
        $language = Language::pluck('name', 'id')->all();
        $author = Author::pluck('name', 'id')->all();
        $publisher = Publisher::pluck('name', 'id')->all();
        $product = Book::with('pictures')->findOrFail($id);
        $getidauthor = $product->author_id;
        $proByAuthor = Book::with('pictures')->orderBy('created_at', 'desc')->where('author_id', '=', $getidauthor)->paginate(4);
        
        $allPublishers = Book::with('pictures')
                        ->where('publisher_id', '=', $id)
                        ->groupBy('id')
                        ->orderBy('books.id', 'desc')->paginate(8);
        
        $allproducts = Book::with('pictures')
                        ->where('is_thumbnail', '=', '0')
                        ->orderBy('books.id', 'desc')->paginate(4);


        return view('publisher_detail', compact('image','allproducts', 'product', 'allPublishers', 'types', 'proByAuthor', 'authors', 'languages', 'publishers', 'type', 'author', 'language', 'publisher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
