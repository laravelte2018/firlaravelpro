<?php

namespace App\Http\Controllers;

use Session;
use App\Book;
use App\Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Order_product;
use Illuminate\Http\Request;

class OrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if (!Session::has('cart')) {
            $types = \App\Type::all();
            $languages = \App\Language::all();
            $publishers = \App\Publisher::all();
            $authors = \App\Author::all();
            return view('shop.checkout', ['products' => null], compact('types', 'languages', 'publishers', 'authors'));
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $types = \App\Type::all();
        $languages = \App\Language::all();
        $publishers = \App\Publisher::all();
        $authors = \App\Author::all();
        return view('shop.checkout', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice], compact('types', 'languages', 'publishers', 'authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        if (!Session::has('cart')) {
            $types = \App\Type::all();
            $languages = \App\Language::all();
            $publishers = \App\Publisher::all();
            $authors = \App\Author::all();
            return view('shop.checkout', ['products' => null], compact('types', 'languages', 'publishers', 'authors'));
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $types = \App\Type::all();
        $languages = \App\Language::all();
        $publishers = \App\Publisher::all();
        $authors = \App\Author::all();
        return view('shop.checkout', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice], compact('types', 'languages', 'publishers', 'authors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'phone' => 'required'
                ], [
            'name.required' => ' The name field is require',
            'email.required' => ' The email field is require',
            'email.email' => ' The email not font',
            'address.required' => ' The address field is require',
            'phone.required' => ' The phone field is require',
        ]);
        ;

        $input = $request->all();
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $input['order_date'] = $year . '/' . $month . '/' . $day;
        $input['shipper_date'] = $year . '/' . $month . '/' . $day;
        $input['password'] = bcrypt($request->password);
        $input['user_id'] = Auth::user()->id;
        $input['is_admin'] = 1;
        $input['is_thumbnail'] = 1;
        $order = new Order();

        if ($order->create($input)) {
            echo "insert order";

            $order_id = DB::getPdo()->lastInsertId();
            foreach (session()->get('cart')->items as $key => $item) {
                $book = Book::findOrFail($item["item"]->id);
//               if($book->remain>$item["quantity"]){
                $order_details = new Order_product();

                $input['quantity'] = $item["quantity"];
                $input['price'] = $item["subprice"];
                $input['order_id'] = $order_id;
                $input['book_id'] = $item["item"]->id;

                if ($order_details->create($input)) {
                    $alert = "<script>alert('New book have added successed ');</script>";
                }
//               }else{
//                  $alert = "<script>alert('The book".$book->name."don't enough provide for you please reduce quantity this book or buy orther book);</script>"; 
//                  echo "sds";
//                  }
            }

            return redirect('/checkout')->with('alert', $alert);
        }

//  return redirect('/checkout');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
//
    }
    

}
