<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Book;
use App\Order_product;
use Illuminate\Support\Facades\Auth;
use App\Wishlist;
use Illuminate\Support\Facades\DB;

class UserManagementController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $users = User::findOrFail($id);
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $users = User::findOrFail($id);
        return view('user.edit', compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        if (trim($request->password) == '') {
            $input = $request->except('password');
        } else {
            $input = $request->all();
            $request->validate([
                'name' => 'required|string|max:255|min:6',
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|min:6|confirmed',
                'address' => 'required|string|max:255|min:6',
             
            ]);
            $input['password'] = bcrypt($request->password);
            if ($file = $request->file('image')) {
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $sub_folder = $year . '/' . $month . '/' . $day . '/';
                $upload_url = 'images/users' . $sub_folder;
                if (!File::exists(public_path() . '/' . $upload_url)) {
                    File::makeDirectory(public_path() . '/' . $upload_url, 0777, true);
                }
                $name = time() . $file->getClientOriginalName();
                $file->move($upload_url, $name);
                $input ['image'] = $upload_url . $name;
            }
        }
        $users = User::findOrFail($id);
        $status = $users->update($input) ? "success" : "fail";
        $message = $status == "success" ? "Success! Your updated successful" : "Failed! Your updated fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('user/' . $id)->with('response', $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function shopppingcart() {

        $orders = DB::table('orders as o')
                        ->select('o.id', 'o.order_date', 'o.shipper_date', 'o.email', 'o.name', 'o.address', 'o.phone')
                        ->where('user_id', '=', 3)->paginate(4);
        return view('user.shoppingcart', compact('orders'));
    }

    public function viewdetails($id) {
        $users = User::findOrFail($id);
        $orders = Order::where('id', '=', $id)->get();
        $orders_details = DB::table('order_products as or')
                        ->select('orders.order_date', 'orders.shipper_date', 'orders.email', 'orders.phone', 'or.quantity', 'or.price', 'books.name', 'books.description')
                        ->join('orders', 'orders.id', '=', 'or.order_id')
                        ->join('books', 'books.id', '=', 'or.book_id')
                        ->where('user_id', '=', $id)->paginate(4);
        return view('user.details', compact('orders_details', 'orders'));
    }

    public function viewWishlist($id) {
        $listProducts = Wishlist::where('user_id', $id)->get();
        $wishlists = array();
        foreach ($listProducts as $key => $product) {
            $wish = Book::with('pictures')
                    ->findOrFail($product->book_id);
            $wish['wish_id'] = $product->id;
            $wishlists[] = $wish;
        }
        //echo var_dump($wishlist[0]->wish_id);
        return view('user.view-wishlist')->with(compact('wishlists'));
    }

    public function deleteProductWishlist($pid) {
        $wish = Wishlist::findOrFail($pid);
        $wish->delete();
        return back()->withInput();
    }

    public function addToWishlist($pid) {
        $wish = new Wishlist;
        $wish = Wishlist::where('book_id',$pid);
        if($wish->count() == 0){
            DB::table('wishlists')->insert([
                'book_id' => $pid,
                'user_id' => Auth::user()->id,
            ]);
        }
        
        return back()->withInput();
    }

}
