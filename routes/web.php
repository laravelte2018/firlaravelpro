<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::post('posts/changeStatus', array('as' => 'changeStatus', 'uses' => 'AdminProductController@changeStatus'));
Route::resource('/ordermanagement', 'AdminOrderController');
Route::post('/ordermanagement/change-status', 'AdminOrderController@changeOrderStatus');
Route::get('/viewshoppingcart', 'UserManagementController@shopppingcart');
Route::get('/viewdetails/{id}', 'UserManagementController@viewdetails');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/show', 'HomeController@show')->name('search');
Route::group(['middleware'=>['admin']], function(){
Route::resource('admin/authors', 'AdminAuthorController',array('as'=>'admin'));
Route::resource('admin/languages', 'AdminLanguageController',array('as'=>'admin'));
Route::resource('admin/types', 'AdminTypeController',array('as'=>'admin'));
Route::resource('admin/publishers', 'AdminPublisherController',array('as'=>'admin'));
Route::resource('admin/products', 'AdminProductsController',array('as'=>'admin'));
Route::resource('admin/users', 'AdminUsersController' ,array('as'=>'admin'));
Route::get('admin/{id}', 'UserManagementController@show');
Route::post('admin/products/{id}/edit/deleteImageProduct', 'AdminProductsController@deleteImageProduct')->name('delete-image');

});
Route::resource('/', 'HomeController');
Route::post('/product_detail/commentProduct', 'HomeController@commentProduct')->name('comment_roduct');
Route::post('/all_product/sortProduct', 'HomeController@sortProduct')->name('sort_product');
Route::get('/product_detail/{id}', 'HomeController@productDetail')->name('product_detail');
Route::resource('/home', 'HomeController');
Route::get('/author_detail/{id}/','MenuController@menuAuthors')->name('author_detail');
Route::get('/type_detail/{id}/','MenuController@menuTypes')->name('type_detail');
Route::get('/language_detail/{id}/','MenuController@menuLanguages')->name('language_detail');
Route::get('/publisher_detail/{id}/','MenuController@menuPublishers')->name('publisher_detail');
Route::get('/all_product', 'HomeController@allProduct')->name('all_product');
Route::get('/checkout', 'HomeController@checkOut')->name('checkout');
Route::resource('user/management', 'UserManagementController');
Route::get('user/{id}', 'UserManagementController@show');
Route::get('user/edit/{id}', 'UserManagementController@edit');
Route::put('user/update/{id}', 'UserManagementController@update');
Route::get('user/wishlist/{id}', 'UserManagementController@viewWishlist',array('as' => 'management.wishlist'));
Route::get('user/wishlist/add/{pid}', 'UserManagementController@addToWishlist',array('as' => 'management.wishlist.add'));
Route::get('user/wishlist/delete/{pid}', 'UserManagementController@deleteProductWishlist',array('as' => 'management.wishlist.delete'));
Route::get('/add-to-cart/{id}', [
    'uses' => 'ProductController@getAddToCart',
]);
Route::get('/delete-cart-product/{id}', [
    'uses' => 'ProductController@removeItem',
    'as' => 'product.removeItem'
]);

Route::get('/delete-cart/', [
    'uses' => 'ProductController@removeCart',
]);

Route::post('/shopping-cart/change-quantity', [
    'uses' => 'ProductController@changeProductQuantity',
    'as' => 'product.changeProductQuantity'
]);

Route::get('shopping-cart', [
    'uses' => 'ProductController@getCart',
    'as' => 'product.shoppingcart'
]);
Route::get('/contact-us', 'HomeController@contactUs')->name('contact_us');
Route::post('/send-feed-back', 'HomeController@sendFeedback')->name('send_feed_back');
Route::get('/about-us', 'HomeController@aboutUs')->name('about_us');
Auth::routes();
Route::resource('checkout', 'OrderController');