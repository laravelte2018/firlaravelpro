/**
 * French translation for bootstrap-datetimepicker
 * Nico Mollet <nico.mollet@gmail.com>
 */
;(function($){
	$.fn.datetimepicker.dates['fr'] = {
		days: ["Monday", "Tuesday", "Wednesday", "Thurday", "Friday", "Saturday ", "Sunday"],
		daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam" ],
		daysMin: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
		months: ["1", "2", "3", "4", "5","6", "7", "8", "9", "10", "11", "12"],
		monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		today: "Today",
		suffix: [],
		meridiem: ["am", "pm"],
		weekStart: 1,
		format: "yyyy-mm-dd"
	};
}(jQuery));
