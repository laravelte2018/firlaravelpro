//////////////////////////////////////////////////////////
//                  Ajax add to shopping cart           //
//////////////////////////////////////////////////////////
$(document).ready(function () {

    $(document).on("click", ".add-to-cart", function (e) {
        var public_path = $('meta[name="public_path"]').attr('content');
        var x, image, price, quantity, subprice;
        var id = $(this).attr('value');
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: public_path + 'add-to-cart/' + id,
            method: 'get',
            data: {
            },
            success: function (result) {
                changeCartInfo(result);

                image = public_path + result.items[id].item.pictures[0].url;
                price = result.items[id].price;
                quantity = result.items[id].quantity;
                subprice = result.items[id].subprice;
                $('#modal-cart-product-image').attr('src', image);
                $('#modal-cart-product-price').html(price);
                $('#modal-cart-product-quantity').html(quantity);
                $('#modal-cart-product-subprice').html(subprice);
                $('#noticeAddCart').modal({
                    show: 'true'
                });
            }});
    });
});
function changeCartInfo(result) {
    var listItem = result.items;
    var x, y, price, quantity, name, description, picture, count = 0;
    var html = "";
    var public_path = $('meta[name="public_path"]').attr('content');
    var cart_link = $('.cart-btn-show').attr('href');
    var cart_foot = '<a class="tg-btnemptycart" href="' + public_path + '/delete-cart" id="clear-all-cart">'
            + '<i class="fa fa-trash-o"></i>'
            + '<span>Clear Your Cart</span>'
            + '</a>'
            + '<br>'
            + '<span class="tg-subtotal">Subtotal: <trong>$</trong><strong class="cart-price">' + result.totalPrice.toFixed(2) + '</strong> </span>'
            + '<div class="tg-btns">'
            + '<a class="tg-btn tg-active" href="' + public_path + '/shopping-cart">View Cart</a>'
            + '</div>';
    for (x in listItem) {
        if (count < 4) {
            price = listItem[x].price;
            quantity = listItem[x].quantity;
            name = listItem[x].item.name;
            description = listItem[x].item.description;
            picture = listItem[x].item.pictures[0].url;
            html += '<div class="tg-minicarproduct">'
                    + '<figure>'
                    + '<img class="img-responsive" width="80" src="' + public_path + '/' + picture + '" alt="image description">'
                    + '</figure>'
                    + '<div class="tg-minicarproductdata">'
                    + '<h5><a href="javascript:void(0);">' + name + '</a></h5>'
                    + '<h6><a href="javascript:void(0);">$ ' + price + "   " + "X" + quantity + '</a></h6>'
                    + '</div>'
                    + '</div>';
            count++;
        } else {
            html += '<p style="text-align: center"><a href="' + public_path + "shopping-cart" + '">View more</a></p>';
            break;
        }

    }



    $('.cart-total-quantity').html(result.totalQuantity);
    $('.cart-total-price').html(result.totalPrice.toFixed(2));
    $('.total-price').html((0.1 * result.totalPrice + result.totalPrice.toFixed(2)));
    $('.tg-minicartbody').html(html);
    $('.tg-minicartfoot').html(cart_foot);
}

//////////////////////////////////////////////////////////
//   Ajax change number of product shopping cart        //
//////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////
//            Ajax delete product of shopping cart      //
//////////////////////////////////////////////////////////


$(document).ready(function () {

    $(document).on("click", ".remove-cart-product", function (e) {
        e.preventDefault();
        var proId = $(this).attr('value');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: "delete-cart-product/" + proId,
            method: 'get',
            data: {
            },
            success: function (result) {
                if (result.items) {
                    changeCartInfo(result);
                    changeCartBoard(result);
                } else {
                    var html = '<div class="tg-description"><p>No products were added to the cart!</p></div>';
                    var html2 = '<h4>No products in your cart</h4>'
                            + '<a class="btn btn-default btn-warning" href="' + $('meta[name="public_path"]').attr('content') + '">Back to Homepage</a>'
                            + '<br><br>';
                    $('.tg-minicart-info').html(html);
                    $('.cart_info').html(html2);
                    $('.cart-total-price').html("$0");
                    $('.cart-total-quantity').html("0");
                    $('.remove-cart').css("display", "none");
                }
            }});
    });
});


function changeCartBoard(result) {
    var listItem = result.items;
    var x, price, quantity, name, description, pictures, dimensions, id, subprice;
    var cart_list = "";
    var public_path = $('meta[name="public_path"]').attr('content');
    var html = "";
    for (x in listItem) {
        price = listItem[x].price;
        subprice = listItem[x].subprice;
        quantity = listItem[x].quantity;
        name = listItem[x].item.name;
        description = listItem[x].item.description;
        pictures = listItem[x].item.pictures[listItem[x].item.pictures.length - 1].url;
        dimensions = listItem[x].item.dimensions;
        id = listItem[x].item.id;
        html += '<tr>'
                + '<td class="cart_product">'
                + '<a href="' + public_path + '/product_detail/' + id + '"><img class="img-responsive" width="150" src="' + pictures + '" alt="' + name + '"></a>'
                + '</td>'
                + '<td class="cart_description">'
                + '<h4><a href="' + public_path + '/product_detail/' + id + '">' + name + '</a></h4>'
                + '<p>Size: ' + dimensions + '</p>'
                + '</td>'
                + '<td class="cart_price">'
                + '<p><strong>' + price + '</strong> $</p>'
                + '</td>'
                + '<td class="cart_quantity">'
                + '<button class="cart_quantity_button">'
                + '<a class="cart_quantity_up cart-btn-plus" href="javascrip:void(0);"><i class="text-warning fa fa-plus"></i></a>'
                + '<input class="cart_quantity_input count" type="text" name="quantity"  pid="' + id + '" value="' + quantity + '" autocomplete="off" size="2">'
                + '<a class="cart_quantity_down cart-btn-minus" href="javascrip:void(0);"><i class="fa fa-minus"></i></a>'
                + '</button>'
                + '</td>'
                + '<td class="cart_total">'
                + '<p class="cart_total_price"><strong>' + subprice + '</strong>$</p>'
                + '</td>'
                + '<td class="cart_delete">'
                + '<a class="cart_quantity_delete remove-cart-product" href="javascript:void(0)" value="' + id + '"><i class="fa fa-times"></i></a>'
                + '</td>'
                + '</tr>';
    }

    $(".list-cart-product").html(html);
}




// ============================================================================
// btn-plus and btn-minus in "#order-detail-content" table
// ============================================================================

$(document).on('click', '.cart-btn-plus', function () {
    var $count = $(this).parent().find('.count');
    var val = parseInt($count.val(), 10);
    if (val < 5) {
        $count.val(val + 1);
        changeByQuantity($count.attr("pid"), $count.val());

    }
    return false;
});
$(document).on('click', '.cart-btn-minus', function () {
    var $count = $(this).parent().find('.count');
    var val = parseInt($count.val(), 10);
    if (val > 1) {
        $count.val(val - 1);
        changeByQuantity($count.attr("pid"), $count.val());

    }
    return false;
});
function changeByQuantity(pid, pquantity) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: "shopping-cart/change-quantity",
        method: 'post',
        data: {
            id: pid,
            quantity: pquantity
        },
        success: function (result) {
            var $subprice = $('.subprice-' + pid);
            $subprice.html(result.items[pid].subprice);
            changeCartInfo(result);

        }});
}