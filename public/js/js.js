// change status when the admin change status
$(document).ready(function () {

    $('body').on('change', '.order-status', function (e) {

        if (confirm("Continue to change status ?")) {
            var public_path = $('meta[name="public_path"]').attr('content');
            var order_id = $(this).attr('order-id');
            var status = $(this).val();
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: public_path + 'ordermanagement/change-status',
                method: 'post',
                data: {
                    order_id: order_id,
                    status: status
                },
                success: function (result) {
                    if (result[0] == true) {
                        alert("Your update success");
                    }else{
                        alert("Your update fail");
                    }
                }});
        }

    });
});